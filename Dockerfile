FROM node:alpine as builder

RUN mkdir /app

WORKDIR /app

COPY . .

RUN yarn

RUN yarn build


FROM nginx:alpine

COPY --from=builder /app/build /usr/share/nginx/html

EXPOSE 80
