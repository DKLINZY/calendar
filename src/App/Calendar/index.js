import React from "react";
import { Store } from "../store";
import Today from "./Today";
import Clock from "./Clock";
import CalendarMonth from "./CalendarMonth";
import "../../Styles/app.calendar.css";

function Calendar() {
  return (
    <div className={"calendar-container"}>
      <Store>
        <Clock />
        <CalendarMonth />
      </Store>
    </div>
  );
}

export default Calendar;
