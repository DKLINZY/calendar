import React, { useState, useEffect, useCallback, useContext } from "react";
import calendar from "node-calendar";
import { StoreContext } from "../store";
import "../../Styles/calendar.calendarmonth.css";

const newCalendar = new calendar.Calendar(6);

export default function () {
  const { store } = useContext(StoreContext);
  const { date } = store;

  const weekdays = ["Sun", "Mon", "Tue", "Wed", "Thu", "Fri", "Sat"];
  const newDate = new Date();

  const [month, setMonth] = useState({
    year: newDate.getFullYear(),
    month: newDate.getMonth(),
  });

  const [monthdays, setMonthdays] = useState(
    newCalendar.monthdayscalendar(newDate.getFullYear(), newDate.getMonth() + 1)
  );

  const [difference, setDifference] = useState(0);

  const updateCalendar = useCallback(() => {
    let newDate = new Date();
    newDate.setMonth(newDate.getMonth() + difference);

    setMonth({
      year: newDate.getFullYear(),
      month: newDate.getMonth(),
    });

    setMonthdays(
      newCalendar.monthdayscalendar(
        newDate.getFullYear(),
        newDate.getMonth() + 1
      )
    );
  }, [difference]);

  useEffect(() => {
    const updateCalendarMonth = setTimeout(() => {
      console.log("update calender month");
      updateCalendar();
    }, 1000 * 60);
    return () => {
      clearTimeout(updateCalendarMonth);
    };
  });

  useEffect(() => {
    updateCalendar();
  }, [updateCalendar]);

  return (
    <div className={"calendar"}>
      <div className={"calender-header"}>
        <div className={"controller"}>
          <div
            className="button"
            onClick={() => {
              setDifference(difference - 1);
            }}
          >
            {"<"}
          </div>
        </div>
        <div className={"month"}>
          {`${calendar.month_name[month.month + 1]} ${month.year}`}
        </div>
        <div className={"controller"}>
          <div
            className="button"
            onClick={() => {
              setDifference(difference + 1);
            }}
          >
            {">"}
          </div>
        </div>
      </div>

      <div className={"days"}>
        {weekdays.map((weekday) => (
          <div key={weekday} className={"weekday"}>
            {weekday}
          </div>
        ))}
        {monthdays.map((week) =>
          week.map((day, index) => {
            return day === 0 ? (
              <div key={day + index}></div>
            ) : day === date && difference === 0 ? (
              <div key={day + index} className={"calendar-day calendar-today"}>
                {day}
              </div>
            ) : (
              <div key={day + index} className={"calendar-day"}>
                {day}
              </div>
            );
          })
        )}
      </div>
    </div>
  );
}
