import React, { useEffect, useContext } from "react";
import calendar from "node-calendar";
import { StoreContext, UPDATE_TODAY } from "../store";
import "../../Styles/calendar.today.css";

export default function Today() {
  const { store, dispatch } = useContext(StoreContext);
  const { date, day, time } = store;

  const updateToday = () => {
    const newDate = new Date();
    dispatch({
      type: UPDATE_TODAY,
      store: {
        date: newDate.getDate(),
        day: newDate.getDay(),
        time: `${("0" + newDate.getHours()).slice(-2)}:${(
          "0" + newDate.getMinutes()
        ).slice(-2)}:${("0" + newDate.getSeconds()).slice(-2)}`,
      },
    });
  };

  useEffect(() => {
    const updateSecond = setTimeout(() => {
      updateToday();
    }, 1000);
    return () => {
      clearTimeout(updateSecond);
    };
  });

  return (
    <div className={"today"}>
      <div className={"day today-text"}>{calendar.day_name[day - 1]}</div>
      <div className={"date today-text"}>{date}</div>
      <div className={"time today-text"}>{time}</div>
    </div>
  );
}
