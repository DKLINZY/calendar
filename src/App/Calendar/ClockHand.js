import React from "react";
import styled, { keyframes } from "styled-components";

const ClockAnimation = () => {
  const HandAnimation = (position, base) => {
    const HandKeyframes = keyframes`
    0% {
      transform: rotate(${(position / base) * 360 + 90}deg);
    }
    100% {
      transform: rotate(${(position / base) * 360 + 90 + 360}deg);
    }
    `;
    return styled.div`
      animation: ${HandKeyframes} ${base}s linear infinite;
    `;
  };
  const newDate = new Date();
  // console.log(newDate);
  const SecondHandDiv = HandAnimation(newDate.getSeconds(), 60);
  const MinuteHandDiv = HandAnimation(
    newDate.getMinutes() * 60 + newDate.getSeconds(),
    60 * 60
  );
  const HourHandDiv = HandAnimation(
    (newDate.getHours() % 12) * 60 * 60 +
      newDate.getMinutes() * 60 +
      newDate.getSeconds(),
    60 * 60 * 12
  );

  return (
    <>
      <SecondHandDiv className={"hand second"}>
        <span></span>
      </SecondHandDiv>
      <MinuteHandDiv className={"hand minute"}>
        <span></span>
      </MinuteHandDiv>
      <HourHandDiv className={"hand hour"}>
        <span></span>
      </HourHandDiv>
    </>
  );
};

export default React.memo(ClockAnimation);
