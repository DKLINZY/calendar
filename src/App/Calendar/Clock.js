import React, { useEffect, useContext } from "react";
import { StoreContext, UPDATE_TODAY } from "../store";
import ClockHand from "./ClockHand";
import "../../Styles/calendar.clock.css";

export default function Clock() {
  const { store, dispatch } = useContext(StoreContext);
  const { day, hour } = store;
  const day_name = [
    "Sunday",
    "Monday",
    "Tuesday",
    "Wednesday",
    "Thursday",
    "Friday",
    "Saturday",
  ];

  const updateToday = () => {
    const newDate = new Date();
    console.log(newDate.getHours());
    dispatch({
      type: UPDATE_TODAY,
      store: {
        date: newDate.getDate(),
        day: newDate.getDay(),
        hour: newDate.getHours(),
      },
    });
  };

  useEffect(() => {
    const updateSecond = setTimeout(() => {
      updateToday();
    }, 1000);
    return () => {
      clearTimeout(updateSecond);
    };
  });

  return (
    <div className={"today"}>
      <div className={"clock-container"}>
        <div className={"panel-container"}>
          <div className={"box"}>
            <div className={"num"}>
              <span>1</span>
            </div>
            <div className={"num"}>
              <span>2</span>
            </div>
            <div className={"num"}>
              <span>3</span>
            </div>
            <div className={"num"}>
              <span>4</span>
            </div>
            <div className={"num"}>
              <span>5</span>
            </div>
            <div className={"num"}>
              <span>6</span>
            </div>
            <div className={"num"}>
              <span>7</span>
            </div>
            <div className={"num"}>
              <span>8</span>
            </div>
            <div className={"num"}>
              <span>9</span>
            </div>
            <div className={"num"}>
              <span>10</span>
            </div>
            <div className={"num"}>
              <span>11</span>
            </div>
            <div className={"num"}>
              <span>12</span>
            </div>
            <div className={"point"}>
              <span></span>
            </div>
            <ClockHand />
          </div>
        </div>
      </div>
      <div className={"day today-text"}>{`${day_name[day]} ${
        hour > 11 ? "P" : "A"
      }M`}</div>
    </div>
  );
}
