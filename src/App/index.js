import React from "react";
import "../Styles/app.index.css";

import Calendar from "./Calendar";

function App() {
  return (
    <>
      <div className={"container"}>
        <Calendar />
      </div>
    </>
  );
}

export default App;
