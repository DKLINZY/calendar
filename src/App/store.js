import React, { createContext, useReducer } from "react";

export const StoreContext = createContext({});

export const UPDATE_TODAY = "UPDATE_TODAY";

const newDate = new Date();
const initData = {
  date: newDate.getDate(),
  day: newDate.getDay(),
  hour: newDate.getHours(),
};

const reducer = (state, action) => {
  switch (action.type) {
    case UPDATE_TODAY:
      return action.store;
    default:
      return state;
  }
};

export const Store = (porps) => {
  const [store, dispatch] = useReducer(reducer, initData);
  return (
    <StoreContext.Provider value={{ store, dispatch }}>
      {porps.children}
    </StoreContext.Provider>
  );
};
